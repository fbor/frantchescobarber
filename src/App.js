
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

import Home from './screens/Home';
import Login from './screens/Login';

export default class App extends Component {
  render() {
    return (
      <View>
        <Login />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    justifyContent: 'flex-start',
    backgroundColor: '#2D2D2D'
  },
  wrapper: {
    marginTop: 60
  }
});
