import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

import Header from '../components/Header';
import Status from '../components/StatusInitial';
import ButtonAgenda from '../components/ButtonAgenda';
import Banner from '../components/Banner';

export default class Home extends Component {
  render() {
    return (
      <View>
        <Header/>
        <View style={styles.wrapper}>
          <Status info="Brow, Você não possui nenhum agendamento."/>
          <ButtonAgenda />
          <Banner />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    marginTop: 60
  }
});
