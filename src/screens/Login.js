import React, { Component } from 'react';
import { ImageBackground, Text, View, Image, StyleSheet, TextInput, TouchableOpacity } from 'react-native';


export default class Login extends Component {
  render () {

    return (
      <ImageBackground source={require('../assets/images/login.png')} style={{width: '100%', height: '100%'}}>
        <Image style={styles.container} source={require('../assets/images/logo.png')} />

        <Text style={styles.label}>
          Digite seu número
        </Text>
        <View style={styles.wrapper}>
          <TextInput style={styles.input} placeholder="(12) 3456-7890"/>
          <TouchableOpacity style={styles.button}>
            <Image source={require('../assets/images/arrow.png')} />
          </TouchableOpacity>
        </View>
      </ImageBackground>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  label: {
    textAlign: 'center',
    color: '#ffffff',
    fontFamily: 'PT Mono',
    fontSize: 18
  },
  input: {
    flex: 6,
    paddingTop: 18,
    paddingBottom: 18,
    paddingRight: 30,
    paddingLeft: 30,
    fontSize: 18,
    fontFamily: 'PT Mono',
    backgroundColor: '#ffffff',
    color: '#363636',
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    marginTop: 30,
    marginLeft: 20
  },
  button: {
    backgroundColor: '#ffffff',
    marginRight: 20,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    marginTop: 30,
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4

  }
})
