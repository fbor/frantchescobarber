
import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';


export default class Header extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <Text style={styles.logo}>Frantchesco Barber</Text>
          <TouchableOpacity style={styles.menu}>
            <Image source={require('../assets/images/menu.png')} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 40
  },
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 10,
    paddingBottom: 40
  },
  logo: {
    color: '#fff',
    fontSize: 18,
    fontFamily: "PT Mono",
  }
})
