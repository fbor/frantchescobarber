import React, { Component } from 'react';
import { View, Image, StyleSheet } from 'react-native';


export default class Banner extends Component {

  render() {
    return(
      <View style={styles.container}>
        <Image source={require('../assets/images/banner.png')} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center'
  }
})
