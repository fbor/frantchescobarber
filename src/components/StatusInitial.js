import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';


export default class StatusInitial extends Component {
  render() {
    return(
      <View style={styles.container}>
        <Text style={styles.info}>{this.props.info}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    marginTop: 10,
    marginBottom: 10,
    paddingLeft: 50,
    paddingRight: 50,
    paddingTop: 30,
    paddingBottom: 30,
    backgroundColor: '#333232'
  },
  date:{},
  info: {
    color: '#fff',
    fontSize: 18,
    fontWeight: "bold",
    fontFamily: "PT Mono",
    textAlign: 'center',
    lineHeight: 30
  }
})
