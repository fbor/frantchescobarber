import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';


export default class ButtonAgenda extends Component {

  render() {
    return(
      <View style={styles.container}>
        <TouchableOpacity style={styles.button}>
          <Text style={styles.textButton}>AGENDAR AGORA</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 40,
    marginBottom: 40
  },
  button: {
    backgroundColor: '#FFFFFF',
    paddingTop: 18,
    paddingBottom: 18,
    paddingLeft: 62,
    paddingRight: 62
  },
  textButton: {
    color: '#2D2D2D',
    textAlign: 'center',
    fontFamily: 'PT Mono',
    fontSize: 18,
  }
})
